from logging import DEBUG

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sx_blog.model import db
from sx_blog.controllers import blog, backstage_management

app = Flask(__name__)
app.secret_key = 'zzj'
app.config[DEBUG] = True

app.register_blueprint(blog.bg)
app.register_blueprint(backstage_management.bm)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:123456@114.215.196.6:3306/sx_onepiece_blog?charset=utf8'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)

# from sx_blog.controllers  import blog
from sx_blog import commands, model
