# import speech_recognition as sr
#
# from aip import AipSpeech
# import requests
# import json
# import sys
# import pyttsx3  # 文字转语音
#
#
# # 语音采样
# def writeAudioFile(rate=16000):
#     r = sr.Recognizer();
#     with sr.Microphone(sample_rate=rate) as source:
#         print("请说话：")
#         audio = r.listen(source)
#
#     with open("../myaudio.wav", "wb") as file:
#         file.write(audio.get_wav_data())
#     print("声音文件录制完毕")
#
#
# # 声音百度AI转文字
# appid = "24131587"
# apikey = "oWoQ60jUCEscoEjxIbl17LeN"
# secretkey = "pWzi1nM1Iu7B95uoEkNq6OkqGrh2N1Wd"
# client = AipSpeech(appid, apikey, secretkey)
#
#
# def audioConvertToTxt(rate=16000):
#     with open("../myaudio.wav", "rb") as file:
#         fileContent = file.read();
#     audiodata = client.asr(fileContent, "wav", rate, {"dev_pid": 1537})
#     print(audiodata)
#     return audiodata
#
#
# tulingurl = "http://openapi.tuling123.com/openapi/api/v2"
# appkey = "d30c3063d9e34d1ca96ff083c8412ca2"
#
#
# def robot(said="没有听清，请再说一遍"):
#     jsonParam = {
#         "reqType": 0,
#         "perception": {
#             "inputText": {
#                 "text": said
#             },
#             "inputImage": {
#                 "url": "imageUrl"
#             },
#             "selfInfo": {
#                 "location": {
#                     "city": "泰安",
#                     "province": "山东",
#                     "street": "凤凰路"
#                 }
#             }
#         },
#         "userInfo": {
#             "apiKey": appkey,
#             "userId": "yan"
#         }
#     }
#     res = requests.post(tulingurl, None, jsonParam)
#     tulingsay = json.loads(res.text)
#     print(tulingsay["results"][0]["values"]["text"])
#     # txtConvertAudio(tulingsay["results"][0]["values"]["text"])
#     return tulingsay["results"][0]["values"]["text"]
#
#
# # 文字转声音
# def txtConvertAudio(data="没有听清楚，请再说一遍"):
#     engine = pyttsx3.init();
#     rate = engine.getProperty("rate")
#     engine.setProperty("rate", rate - 10)
#     engine.say(data)
#     print(data)
#     engine.runAndWait()
#
#
# if __name__ == "__main__":
#     i = 0
#     while True:
#         i = i + 1
#         print("第", str(i), "句")
#         writeAudioFile(16000)
#         audiotxt = audioConvertToTxt()
#         saycontent = robot(audiotxt)
#         txtConvertAudio(saycontent)
#         if i == 5:
#             sys.exit()
#
# # robot("今天的天气预报")
# # writeAudioFile()
# # audioConvertToTxt()
