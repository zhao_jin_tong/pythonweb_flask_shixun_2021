import json

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, jsonify
)
# from flask_pagedown import PageDown
from sx_blog.model import Blog, db, blog_type
from sx_blog.SpeakModel import *

bg = Blueprint('blog', __name__, url_prefix='/blog')


@bg.route("/", methods=('GET', 'POST'))
def index():
    bg = Blog.query.all()
    zxbg = Blog.query.limit(4).all()
    # zxbg = Blog.query.all()
    return render_template("index.html", zxblog=zxbg, blog=bg)


@bg.route("/blogpage/<blog_id>", methods=('GET', 'POST'))
def blogpage(blog_id):
    print(blog_id)
    blog_id = a= int(blog_id)
    bg = Blog.query.get(blog_id)
    type = blog_type.query.get(bg.type_id)
    return render_template("blog.html", bg=bg, type=type.name)


@bg.route("/blogpage/content", methods=('GET', 'POST'))
def content():
    data = request.form.get('url')
    data = data.split('/')
    id = int(data[len(data) - 1])
    # print(id)
    bg = Blog.query.get(id)
    # dist = {}
    # print(bg.content)
    return bg.content


@bg.route("/type", methods=('GET', 'POST'))
def type():
    ty = blog_type.query.all()
    zxty = blog_type.query.limit(4).all()
    return render_template("types.html", zxtype=zxty, type=ty)




@bg.route("/markdown", methods=('GET', 'POST'))
def markdown():
    return render_template("markdown.html")


@bg.route("/biaodan", methods=('GET', 'POST'))
def biaodan():
    bg = Blog.query.first()
    # val = request.form.get("edi")
    text = request.form.get("text")
    bg.content = text
    db.session.commit()
    # html = request.form.get("html")
    # print("val=${}", val)
    # print("============================================================================")
    # print("============================================================================")
    # print("text=${}", text)
    # print("============================================================================")
    # print("============================================================================")
    # print("html=${}", html)

    return "success"


@bg.route("/label", methods=['POST', 'GET'])
def label():
    return render_template("label.html")


@bg.route("/remarkAdd", methods=['POST', 'GET'])
def remarkAdd():
    id = request.form.get('id')
    f_id = request.form.get('f_id')
    name = request.form.get('nickname')
    email = request.form.get('email')
    content = request.form.get('content')
    time = request.form.get('time')
    print(time)


@bg.route("/types/<int:type_id>", methods=['POST', 'GET'])
def types(type_id):
    type = blog_type.query.all()
    types = []
    for i in type:
        dis = {'name': i.name, 'num': len(Blog.query.filter(Blog.type_id == i.id).all()), 'id': i.id}
        types.append(dis)
    if type_id == 0:
        type_id = (blog_type.query.first()).id
    blog = Blog.query.filter(Blog.type_id == type_id).all()
    name = blog_type.query.get(type_id).name
    return render_template("types.html", type=types,bloglist = blog,url=type_id,name=name)

@bg.route("/about",methods=['POST','GET'])
def about():
    return render_template("about.html")
@bg.route("/file",methods=['POST','GET'])
def file():
    bg = Blog.query.all()
    return render_template("file.html",blog=bg)

#@bg.route("/say",methods=['POST','GET'])
#def say():
 #   saycontent=audioConvertToTxt()
  #  return saycontent
@bg.route("/say")
def say():
    saycontent=robot()
    # print(saycontent)
    return saycontent


