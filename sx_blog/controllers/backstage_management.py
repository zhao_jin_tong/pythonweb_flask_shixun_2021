import datetime

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, jsonify
)
from sx_blog.model import db, user, Blog, blog_type

bm = Blueprint('backstage', __name__, url_prefix='/backstage')


@bm.route('login', methods=['POST', 'GET'])
def login():
    return render_template("login_register.html")


@bm.route("ta_login", methods=["post"])
def ta_login():
    '''这是个登录功能'''
    print("success")
    uname = request.form.get("l_uname", "")
    upwd = request.form.get("l_upwd", "")
    # print(request.form.__str__())
    a = user.query.filter(user.username == uname, user.password == upwd).all()
    print(a)
    if len(a) == 0:
        return "0"
    else:
        return "1"


@bm.route("manager", methods=["post", "get"])
def manager():
    return render_template("admin.html")


@bm.route("articleList", methods=['POST', 'GET'])
def articleList():
    return render_template("articleList.html")


@bm.route("/findArticleList")
def findArticleList():
    limit = request.args.get("limit")
    page = request.args.get("page")
    rs = Blog.query.all()
    data = []
    for i in rs:
        d = {}
        d['id'] = i.id
        d['title'] = i.title
        d['describe'] = i.describe
        d['creation_time'] = i.creation_time
        type = blog_type.query.get(i.type_id).name
        d['type'] = type
        data.append(d)
    # print(data)
    return jsonify({'count': len(data), 'data': data[int(page) - 1:(int(page) + int(limit))], 'code': 0})


@bm.route("articleAdd", methods=['POST', 'GET'])
def articleAdd():
    a = blog_type.query.all()
    return render_template("articleAdd.html", type=a)


@bm.route("Addarticle", methods=['POST', 'GET'])
def Addarticle():
    text = request.form.get("text")
    type = request.form.get("type")
    title = request.form.get("title")
    print(title)
    describe = request.form.get("describe")
    now = datetime.datetime.now()
    temp = Blog(content=text, type_id=type, title=title, describe=describe, creation_time=now, views_number=0,
                user_id=1)
    db.session.add(temp)
    db.session.commit()
    return "success"


@bm.route("typeList", methods=['POST', 'GET'])
def typeList():
    return render_template("typeList.html")


@bm.route("/findTypeList")
def findTypeList():
    limit = request.args.get("limit")
    page = request.args.get("page")
    rs = blog_type.query.all()
    data = []
    for i in rs:
        d = {}
        d['id'] = i.id
        d['name'] = i.name
        data.append(d)
    print(data)
    return jsonify({'count': len(data), 'data': data[int(page) - 1:(int(page) + int(limit))], 'code': 0})


@bm.route("welcome", methods=['POST', 'GET'])
def welcome():
    return render_template("welcome.html")


@bm.route("/getUnivTagsDataForPie")
def getUnivTagsDataForPie():
    a = Blog.query.limit(4).all()
    list = []
    for item in a:
        dict = {}
        dict["name"] = item.title
        dict["value"] = item.views_number
        list.append(dict)
    print(list)
    return jsonify(list)


@bm.route("/deleteUnivById", methods=["post"])
def deleteUnivById():
    id = request.form.get("id")
    b = Blog.query.get(id)
    db.session.delete(b)
    db.session.commit()
    print("success")
    return "1"
# @bm.route("/login",methods=["post"])
# def login():
#   '''这是个登录功能'''
#  uname=request.form.get("l_uname","")
# upwd=request.form.get("l_upwd","")
# print(request.form.__str__())


# conn=MySQLHelper();
# sql="select * from tb_user where uname=%s and upwd=md5(%s)"
# rs=conn.executeForSelectAll(sql,(uname,upwd))
# i=rs.__len__()
# print(i)
# if i==0:
#     return "0"#同步请求 fault
# else:
#     return "1"  # success
