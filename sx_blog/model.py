# from sx_blog import db
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


# class Blog(db):
class Blog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # 内容
    content = db.Column(db.Text)
    # 标题
    title = db.Column(db.String(225))
    # 描述
    describe = db.Column(db.Text)
    # 更新时间
    update_time = db.Column(db.DateTime)
    # 创建时间
    creation_time = db.Column(db.DateTime)
    # 图片
    picture = db.Column(db.String(255))
    # 浏览次数
    views_number = db.Column(db.Integer)
    # 类型ID
    type_id = db.Column(db.Integer)
    # 作者ID
    user_id = db.Column(db.Integer)
    # _id = db.Column(db.Integer)
    # blog_type = db.Column(db.Integer)


class blog_type(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))


class blog_label(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))


class relationship_blog_label(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    blog_id = db.Column(db.Integer)
    label = db.Column(db.Integer)


class remark(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    email = db.Column(db.String(255))
    content = db.Column(db.String(2048))
    time = db.Column(db.String(255))
    blog_id = db.Column(db.String(255))
    father_id = db.Column(db.String(255))

class user(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255))
    password = db.Column(db.String(255))